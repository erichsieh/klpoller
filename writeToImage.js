const webshot = require('webshot');
const fs = require('fs');

const renderRow = (stockItem, rowClass) => {
    const {
        item,
        price,
        quantity,
        date
    } = stockItem;

    return `<tr class="${rowClass}"><td>${item}</td><td class="quantity">${quantity}</td><td class="price">${price}</td></tr>`;
};

module.exports = function parseKLFeed(response, callback) {
    const fullOutput = [];
    // list of in stock products independent of quantity (only care about newly in stock stuff)
    const instockProducts = [];
    const newInstockProducts = [];

    const fullHtmlOutput = ['<html><head><style type="text/css">'];
    fullHtmlOutput.push('body { font-family: sans-serif; font-size: 15pt; }.date {width: 50px} .newinstock {background-color: #3CB371;} .instock {background-color: #ADD8E6;}td {border: solid 1px;margin: 0;padding: 5px;font-family: sans-serif; font-size:12px}table {border-collapse: collapse; width: 100%;}');
    fullHtmlOutput.push('.price { text-align: right; }');
    fullHtmlOutput.push('.quantity { text-align: right; }');
    fullHtmlOutput.push('.date { text-align: right; width: 60px}');
    fullHtmlOutput.push('</style></head><body>');
    fullHtmlOutput.push(`<table>`);

    const rowClass = 'instock';

    response.newInStockItems && response.newInStockItems.forEach((stockItem) => fullHtmlOutput.push(renderRow(stockItem, 'newinstock')));
    response.inStockItems.forEach((stockItem) => fullHtmlOutput.push(renderRow(stockItem, 'instock')));

    fullHtmlOutput.push('</table></body></html>');

    webshot(fullHtmlOutput.join(''), 'foo.png',
        {
            siteType: 'html',
            windowSize: { width: '375', height: '640'},
            shotSize: { width: 'window', height: 'window'},
            userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
        }, function(err) {
        console.log('finished webshot foo.png');
    });

    fs.writeFile("/Users/ehsieh/Documents/klpoller/output.html", fullHtmlOutput.join(''), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});

    callback();
};
