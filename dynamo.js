const AWS = require('aws-sdk');

// Lambda will autoload AWS credentials
// AWS.config.loadFromPath('./config.json');

const dynamodb = new AWS.DynamoDB();

function getFromDB(callback) {
    const params = {
        TableName: 'klpoller',
        Key: {
            id: { S: 'klfeed' }
        }
    };

    dynamodb.getItem(params, (err, data) => {
        if (err) {
            console.log("Error", err);
        } else {
            callback(data.Item);
        }
    });
}

function writeToDB({hash, inStock = []}) {
    const inStockItems = inStock.map((item) => item.item).join('|');

    const params = {
        TableName: 'klpoller',
        Item: {
            id: {S: 'klfeed'},
            timestamp: {S: Date.now()+'' },
            hash: {S: hash},
            inStockItems: { S: inStockItems }
        }
    };

    console.log(`writeToDB params=${JSON.stringify(params)}`);

    dynamodb.putItem(params, function(error, resp) {
        if (error) {
            console.log(error);
        }
    });
}

module.exports = {
    getFromDB,
    writeToDB
};
