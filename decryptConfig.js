const AWS = require('aws-sdk');

const encrypted = {
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    accessToken: process.env.TWITTER_ACCESS_TOKEN,
    accessTokenSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET
};

const DISABLE_TWEETS = process.env.DISABLE_TWEETS === 'true';

const KL_FEED_URL = process.env.KL_FEED_URL;

let decrypted = {
    DISABLE_TWEETS,
    KL_FEED_URL
};

//
// Retrieve encrypted credentials from KMS.
//
module.exports = (callback) => {
    if (decrypted.consumerKey && decrypted.consumerSecret && decrypted.accessToken && decrypted.accessTokenSecret) {
        return callback(null, decrypted);
    } else {
        const kms = new AWS.KMS();

        const decryptPromises = [
            kms.decrypt( { CiphertextBlob: new Buffer(encrypted.consumerKey, 'base64') } ).promise(),
            kms.decrypt( { CiphertextBlob: new Buffer(encrypted.consumerSecret, 'base64') } ).promise(),
            kms.decrypt( { CiphertextBlob: new Buffer(encrypted.accessToken, 'base64') } ).promise(),
            kms.decrypt( { CiphertextBlob: new Buffer(encrypted.accessTokenSecret, 'base64') } ).promise()
        ];

        Promise.all( decryptPromises ).then( data => {
            decrypted.consumerKey = data[0].Plaintext.toString('ascii');
            decrypted.consumerSecret = data[1].Plaintext.toString('ascii');
            decrypted.accessToken = data[2].Plaintext.toString('ascii');
            decrypted.accessTokenSecret = data[3].Plaintext.toString('ascii');

            return callback(null, decrypted);
        }).catch( err => {
            console.log('Decrypt error:', err);
            return callback(err);
        });
    }
};