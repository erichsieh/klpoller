const parseKLFeed = require('./parsekl');
const sendEmailUpdate = require('./sendEmailUpdate');
const { getFromDB, writeToDB } = require('./dynamo');
const writeToImage = require('./writeToImage');
const postTweet = require('./postTweet');
const parseInStock = require('./parseInStock');

// main
getFromDB((item) => {
    const prevInStock = parseInStock(item);

    console.log(`prevInStock=${JSON.stringify(prevInStock, null, 2)}`);
    parseKLFeed(prevInStock, (err, response) => {
        if (!err) {
            const oldHash = item && item.hash && item.hash.S;

            console.log(`Comparing old hash ${oldHash} to new hash ${response.hash}`);
            // check if there are new changes
            console.log(JSON.stringify(response, null, 2));
            writeToImage(response, (error, data) => {
                if (!error) {

                }
                console.log('done writeToImage');
            });
            // if (oldHash !== response.hash) {
            //     postTweet(response, (tweetError, data) => {
            //         if (!tweetError) {
            //             writeToDB(response);
            //             console.log('finished sending update and writing to db');
            //         } else {
            //             console.log('error sending tweet');
            //             console.log(tweetError);
            //         }
            //     });
            // } else {
            //     console.log('no updates');
            // }
        } else {
            console.log('Parse feed error');
            console.log(err);
        }
    });
});