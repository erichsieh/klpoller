module.exports = (item) => {
    const prevInStock = item && item.inStockItems && item.inStockItems.S && item.inStockItems.S.split('|').reduce((agg, item) => {
        if (item && item.length > 0) {
             agg[item] = 1;
        }
        return agg;
    }, {});

    return prevInStock;
};