module.exports = (mainCallback) => (err, config) => {
    if (err) {
        console.log('Decrypt config error');
        console.log(err);
        return mainCallback(err);
    }

    console.log(`CONFIG=${JSON.stringify(config)}`);

    const parseKLFeed = require('./parsekl');
    const postTweet = require('./postTweet')(config);

    const { getFromDB, writeToDB } = require('./dynamo');
    const parseInStock = require('./parseInStock');

    // main
    getFromDB((item) => {
        const prevInStock = parseInStock(item);

        parseKLFeed(config.KL_FEED_URL, prevInStock, (err, response) => {
            if (!err) {
                const oldHash = item && item.hash && item.hash.S;

                console.log(`Comparing old hash ${oldHash} to new hash ${response.hash}`);
                // check if there are new changes
                // console.log(JSON.stringify(response, null, 2));
                if (oldHash !== response.hash) {
                    if (!config.DISABLE_TWEETS) {
                        postTweet(response, (tweetError, data) => {
                            if (!tweetError) {
                                writeToDB(response);
                                console.log('finished sending update and writing to db');
                                mainCallback(null, 'finished sending update and writing to db');
                            } else {
                                console.log('error sending tweet');
                                console.log(tweetError);
                                mainCallback(null, 'error sending tweet');
                            }
                        });
                    } else {
                        mainCallback(null, 'Tweets are disabled, returning');
                    }
                } else {
                    console.log('no updates');
                    mainCallback(null, 'no updates');
                }
            } else {
                console.log('Parse feed error');
                console.log(err);
                mainCallback(err);
            }
        });
    });
}