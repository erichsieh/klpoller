const Twit = require("twit");

const MAX_CHAR_COUNT = 280;
const MAX_LINK_LENGTH = 25;

module.exports = config => (data, callback) => {
    const {
        consumerKey,
        consumerSecret,
        accessToken,
        accessTokenSecret
    } = config;

    var T = new Twit({
        consumer_key: consumerKey,
        consumer_secret: consumerSecret,
        access_token: accessToken,
        access_token_secret: accessTokenSecret
        // ,
        // timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
    });

    const postUpdate = function(items, replyId, cb) {
        if (!items && items.length === 0) {
            cb();
        } else {
            const status = items.shift();

            const payload = { status };
            if (replyId) {
                payload.in_reply_to_status_id = replyId;
            }

            console.log(`Posting update ${JSON.stringify(payload)}`);

            T.post("statuses/update", payload, function(err, data, response) {
                // console.log(JSON.stringify(data));
                if (err) {
                    cb(err);
                } else {
                    if (items.length === 0) {
                        cb(undefined, data);
                    } else {
                        postUpdate(items, data.id_str, cb);
                    }
                }
            });
        }
    };

    // console.log(`postTweet ${JSON.stringify(data, null, 2)}`);
    const outputArr = data.newInstockItems;

    // break up items to tweet length
    // trying to insert the kl feed link in the first tweet
    const tweetOutput = [];

    let output = "";

    let linkAdded = false;

    outputArr.map(({ item, price, quantity }) => {
        const itemStr = `${item} ${price} ${quantity}`;
        const itemLength = itemStr.length + 1;
        const currentLength = output.length;

        const len = output.length + itemStr.length + 1;

        // console.log(`itemStr=${itemStr} currentLength=${currentLength} itemLength=${itemLength} len=${currentLength + itemLength + MAX_LINK_LENGTH}`);
        if (currentLength === 0) {
            // new tweet
            output = itemStr;
        } else if (
            !linkAdded &&
            currentLength + itemLength + MAX_LINK_LENGTH > MAX_CHAR_COUNT
        ) {
            // adding the next item will be too long with the link so just add the link
            output += `\n${data.feedUrl}`;
            tweetOutput.push(output);
            output = itemStr;
            linkAdded = true;
        } else if (currentLength + itemLength < MAX_CHAR_COUNT) {
            output += `\n${itemStr}`;
        } else {
            tweetOutput.push(output);
            output = itemStr;
        }
    });

    if (output.length > 0) {
        if (!linkAdded && output.length + MAX_LINK_LENGTH <= MAX_CHAR_COUNT) {
            output += `\n${data.feedUrl}`;
        }
        tweetOutput.push(output);
    }

    tweetOutput.forEach(item => {
        console.log(item);
        console.log(item.length);
    });

    if (tweetOutput.length > 0) {
        postUpdate(tweetOutput, undefined, function(err, data) {
            if (err) {
                console.log(`Error posting tweet ${err}`);
                callback(err);
            } else {
                callback(undefined, data);
            }
        });
    } else {
        console.log("no tweet needed");
        callback();
    }
};
