exports.handler = (event, context, mainCallback) => {
    const main = require('./main')(mainCallback);
    // for encrypted keys using kms
    // const decryptConfig = require('./decryptConfig');
    // get plaintext keys from process.ENV
    const config = require('./config');

    // decrypt the config first before running the main loop
    config(main);
};