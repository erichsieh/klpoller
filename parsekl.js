const request = require('request');
const cheerio = require('cheerio');
const md5 = require('md5');
const moment = require('moment-timezone');

const DEFAULT_URL = 'https://www.klwines.com/Products?&filters=-_x-_v%22p%22-_i%22NewProductFeedYN%22-_q%22o%22-_i%22eq%22-_q%22v%22-_i1-_q%22vi%22-_itrue-_q%22t%22-_i%22ProductFeed%22-_q%22f%22-_i-_xx_-v_--_q-_v%22t%22-_i%22dflt-stock-all%22v_--_q-_v%22p%22-_i%2230%22-_q%22o%22-_i%22eq%22-_q%22v%22-_i%22Distilled+Spirits%22-_q%22vi%22-_itrue-_q%22t%22-_i%22ff-30-Distilled+Spirits--%22-_q%22f%22-_i-_xx_-v_-x_-&limit=50&offset=0&orderBy=NewProductFeedDate%20desc&searchText=';
const baseUrl = 'http://www.klwines.com';

module.exports = function parseKLFeed(klUrl = DEFAULT_URL, prevNewItems = {}, callback) {
    console.log(`Fetching URL ${klUrl}`);

    request(klUrl, function(error, response, html) {
        if (!error) {
            var $$ = cheerio.load(html);
            var feed = $$('.col-b tbody');
            var rows = feed.children('tr');

            // list of in stock products independent of quantity (only care about newly in stock stuff)
            const newInstockItems = [];
            const inStockItems = [];
            const oosItems = [];

            rows.each((i, elem) => {
                var tds = $$(elem).children('td');

                const date = $$('.formatDate', elem).text();
                const dateObj = new Date(date);
                const adjustedDate = new Date(Date.UTC(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate(), dateObj.getHours(), dateObj.getMinutes(), dateObj.getSeconds()));
                const dateStr = moment(adjustedDate).tz('America/Los_Angeles').format('M/D/YY h:mma');

                const link = $$(':nth-child(4) a', elem);
                const href = $$(link).attr('href');
                const itemLabel = $$(link).text().trim();

                const price = $$(':nth-child(5)', elem).text();
                const quantity = $$(':nth-child(6)', elem).text();

                const instock = quantity !== 'Sold Out';
                const newInstock = !!!prevNewItems[itemLabel] && instock;

                const item = {
                    item: itemLabel,
                    price,
                    quantity,
                    date: dateStr,
                    link: baseUrl + href
                };

                if (newInstock) {
                    newInstockItems.push(item);
                } else if (instock) {
                    inStockItems.push(item);
                } else {
                    oosItems.push(item);
                }
            });

            const hash = md5(newInstockItems.map((item) => item.item).join(''));

            const result = {
                inStock: newInstockItems.concat(inStockItems),
                inStockItems,
                newInstockItems,
                oosItems,
                hash,
                feedUrl: klUrl
            };

            callback(null, result);
        } else {
            callback(error);
        }
    });
}