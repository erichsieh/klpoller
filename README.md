1. Zip up repo
git archive -o klpoller.zip HEAD -v

To archive unstaged files

- stashName=`git stash create`
- git archive -o klpoller.zip $stashName -v

2. Install node s3-cli

npm install -g s3-cli

3. Add AWS credentials for node aws-sdk module

Add to ~/.aws/credentials

https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-shared.html

https://www.npmjs.com/package/s3-cli

4. To upload to s3

s3-cli put klpoller.zip s3://twobearluge/klpoller.zip --region=us-west-2 --config ~/.aws/credentials

5. When updating lambda, set environment variables

KL_URL
TWITTER_CONSUMER_KEY
TWITTER_CONSUMER_SECRET
TWITTER_ACCESS_TOKEN
TWITTER_ACCESS_TOKEN_SECRET
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY

6. The user that runs the lambda needs permissions to dynamo and KMS (Key Management Store)