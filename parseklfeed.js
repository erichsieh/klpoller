const request = require('request');
const cheerio = require('cheerio');
const md5 = require('md5');
const dateformat = require('dateformat');
const moment = require('moment-timezone');

const klUrl = 'http://www.klwines.com/productfeed?&productTypeCD=10&minprice=&maxprice=&page=1';
const baseUrl = 'http://www.klwines.com';

module.exports = function parseKLFeed(prevNewItems = {}, callback) {
    request(klUrl, function(error, response, html) {
        if (!error) {
            var $$ = cheerio.load(html);

            var feed = $$('.col-b tbody');
            var rows = feed.children('tr');

            const fullOutput = [];
            // list of in stock products independent of quantity (only care about newly in stock stuff)
            const instockProducts = [];
            const newInstockProducts = [];

            const fullHtmlOutput = ['<html><head><style type="text/css">'];
            fullHtmlOutput.push('.newinstock {font-weight: bold;background-color: #3CB371;} .instock {font-weight: bold;background-color: #ADD8E6;}td {border: solid 1px;margin: 0;padding: 5px;}table {border-collapse: collapse;}');
            fullHtmlOutput.push('</style></head><body>');
            fullHtmlOutput.push(`<div><a href="${klUrl}">K&L Feed</a></div><table>`);

            const newInstockItems = [];
            const newInstockOutput = [];
            const newInstockHtmlOutput = [];
            const instockOutput = [];
            const instockHtmlOutput = [];
            const oosOutput = [];
            const oosHtmlOutput = [];

            rows.each((i, elem) => {
                var tds = $$(elem).children('td');

                const date = $$('.formatDate', elem).text();
                const dateObj = new Date(date);
                const adjustedDate = new Date(Date.UTC(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate(), dateObj.getHours(), dateObj.getMinutes(), dateObj.getSeconds()));
                const dateStr = moment(adjustedDate).tz('America/Los_Angeles').format('M D YY, h:mma');

                var link = $$(':nth-child(4) a', elem);
                const href = $$(link).attr('href');
                const itemLabel = $$(link).text();

                const price = $$(':nth-child(5)', elem).text();
                const quantity = $$(':nth-child(6)', elem).text();

                const instock = quantity !== 'Sold Out';
                const newInstock = !!!prevNewItems[itemLabel] && instock;

                let rowClass = '';

                if (newInstock) {
                    rowClass = 'newinstock';
                } else if (instock) {
                    rowClass = 'instock';
                }

                const output = [dateStr, price, quantity, baseUrl + href, itemLabel];
                const htmlOutput = [`<tr class="${rowClass}">`, `<td>${dateStr}</td>`, `<td>${quantity}</td>`, '<td><a href="' + baseUrl + href + '">' + itemLabel + '</a></td>', `<td>${price}</td>`, '</tr>'];

                if (instock) {
                    if (newInstock) {
                        newInstockItems.push([itemLabel, price, quantity]);
                        newInstockOutput.push(output.join(' '));
                        newInstockHtmlOutput.push(htmlOutput.join(' '));
                        instockProducts.push(itemLabel);
                        newInstockProducts.push(itemLabel);
                    } else {
                        instockOutput.push(output.join(' '));
                        instockHtmlOutput.push(htmlOutput.join(' '));
                        instockProducts.push(itemLabel);
                    }
                } else {
                    oosOutput.push(output.join(' '));
                    oosHtmlOutput.push(htmlOutput.join(' '));
                }
            });

            const fullOutputArr = newInstockOutput.concat(instockOutput).concat(oosOutput);
            const fullOutputString = fullOutputArr.join('');

            const outputHash = md5(newInstockProducts.join(''));

            const completeHtmlOutput = fullHtmlOutput.concat(newInstockHtmlOutput).concat(instockHtmlOutput).concat(oosHtmlOutput);
            completeHtmlOutput.push('</table></body></html>');

            // console.log('new in stock ' + JSON.stringify(newInstockOutput));
            // console.log(completeHtmlOutput.join(''));
            const now = new Date();
            const formattedDate = moment(now).tz('America/Los_Angeles').format('MMMM Do YYYY, h:mm:ss a');

            callback(null, {
                subject: `${formattedDate} K&L Inventory update`,
                output: fullOutputArr,
                htmlOutput: completeHtmlOutput,
                outputHash,
                inStockItems: instockProducts,
                newInstockItems,
                newInstockProducts,
                klUrl
            });
        } else {
            callback(error);
        }
    });
}